import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Invoice} from './invoice';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {

  invoice:Invoice;

  constructor() { }

  ngOnInit() {
  }
  

}
