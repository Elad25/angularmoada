import { Component, OnInit} from '@angular/core';
import {ProductsService} from './products.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
products;

  constructor(private _productsService: ProductsService) { }

  ngOnInit() {this._productsService.getProducts()
 			    .subscribe(products => {this.products = products;console.log(products)});
  }

   deleteUser(product){
      this._productsService.deleteUser(product);
  }

 editProduct(product){
     this._productsService.updateProduct(product); 
  }  
  

}
