import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service'

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css'],
   styles: [`
    .users li { cursor: default; border-style: groove;; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #F0F8FF;
         border-color: #ecf0f1; 
         color: #2c3e50;
         border: solid ;
    }     
  `]
})
export class InvoicesComponent implements OnInit {

  invoices;
  currentUser;
  isLoading:Boolean = true;

  constructor(private _invoicesService: InvoicesService) { }

  ngOnInit() {
           this._invoicesService.getInvoice()
			    .subscribe(invoicesData => {this.invoices = invoicesData; console.log(this.invoices); this.isLoading = false});
  
  }

  addInvoice(invoice){
    this._invoicesService.addInvoice(invoice);
  }

select(user){
		this.currentUser = user; 
    console.log(	this.currentUser);
 }

}
