import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {

  invoiceObservable;

  constructor(private af:AngularFire) { }

  addInvoice(invoice){
      this.invoiceObservable.push(invoice);
  }

  getInvoice(){
    this.invoiceObservable = this.af.database.list('/Invoices');
    return this.invoiceObservable;
  }

}

