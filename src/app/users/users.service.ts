import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {


usersObservable;
  constructor(private af:AngularFire, private _http:Http) { }

    addUser(user){
    this.usersObservable.push(user);
  }
  getUsers(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return users;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
	}

    deleteUser(user){
    this.af.database.object('/users/' + user.$key).remove();
    console.log('/users/' + user.$key);
  }
    

  updateUser(user){
    let user1 = {name:user.name,email:user.email}
    console.log(user1);
    this.af.database.object('/users/' + user.$key).update(user1)
  }


}
