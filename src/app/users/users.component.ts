import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users;
currentUser;
  constructor(private _usersService: UsersService) { }


  addUser(user){
    this._usersService.addUser(user);
  }

  ngOnInit() {    this._usersService.getUsers()
 			    .subscribe(users => {this.users = users;console.log(users)});
  }
   

     deleteUser(user){
      this._usersService.deleteUser(user);
  }
      editUser(user){
      this._usersService.updateUser(user);
      console.log(this.users);
  }

   select(user){
		this.currentUser = user; 
    //console.log(	this.currentUser);
 }

}
