import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { FormComponent } from './form/form.component';
import { UsersService  } from './users/users.service';
import { InvoicesService  } from './invoices/invoices.service';

import { AngularFireModule } from 'angularfire2';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';


const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'Invoices', component: InvoicesComponent },
  { path: 'InvoiceForm', component: InvoiceFormComponent },
  { path: '', component: InvoicesComponent },
  { path: '**', component: PageNotFoundComponent }
];

  var config = {
    apiKey: "AIzaSyCFaa4hO2r82LLZdQPH1aNevx_y3K2q1p0",
    authDomain: "practice-84428.firebaseapp.com",
    databaseURL: "https://practice-84428.firebaseio.com",
    storageBucket: "practice-84428.appspot.com",
    messagingSenderId: "561283701502"
  };

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    FormComponent,
    ProductsComponent,
    ProductComponent,
    PageNotFoundComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(config),
    RouterModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UsersService, ProductsService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }